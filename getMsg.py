#coding=utf-8
import urllib2
import json
import sqlite3
import re
import socket
import time
import threading

gprmc = []

def f(string,width):
	return [string[x:x+width] for x in range(0,len(string),width)]

# def getJokes():
# 	while 1:
# 		getJoke()
# 		time.sleep(600)

def getJoke():
	try:
		response = urllib2.urlopen("http://japi.juhe.cn/joke/content/text.from?page=10&pagesize=20&key=bf1ec1946b1d9fe99e62ca91a194905b")
		jokes = json.loads(response.read());

		if jokes["error_code"] != 0:
			return
		#连接数据库
		conn = sqlite3.connect("/home/pi/msg.db")

		#内容拆分成60个字符为一条
		for joke in jokes["result"]["data"]:
			msg = joke["content"].replace("\n","")
			msg = msg.replace("\'","")
			msg = msg.replace(" ","")
			rows = f(msg,60)
			for row in reversed(rows):
				print row
				#print "insert into t_msg(fmsg) select '"+row+"' where not exists(select * from t_msg where fmsg = '"+row+"')"
				conn.execute("insert into t_msg(fmsg) select '"+row+"' where not exists(select * from t_msg where fmsg = '"+row+"')")

		#保存并写入数据库
		conn.commit()
		conn.close()
	except BaseException,e:
		print ("读取Joke 信息失败",e)
		return -1
	else:
		print "读取Joke 信息成功"
		return 0

def convertGPS(gpsinfo,type):
	if type == "N":
		return "%0.8f" % (int(gpsinfo[0:2]) + float(gpsinfo[2:]) / 60)
	elif type == "E":
		return "%0.8f" % (int(gpsinfo[0:3]) + float(gpsinfo[3:]) / 60)

def convertTime(timestr,datestr):
	a = "20"+datestr[4:]+"-"+datestr[2:4]+"-"+datestr[0:2] + " "
	a += timestr[0:2] + ":" + timestr[2:4] + ":" + timestr[4:6]
	return "%d" %(time.mktime(time.strptime(a,'%Y-%m-%d %H:%M:%S')))

#获取当前道路
def getCurrentRoad():
	
	speed = ""
	locations = ""
	times     = ""
	direction = ""
    #speed = ""
	
	#定位信息不足
	if len(gprmc) < 3:
		print("GPS信息不足！")
		return

	for i in range(3):
		gpsinfo = gprmc.pop(0).split(",")
		locations 	+= convertGPS(gpsinfo[5],"E") + "," + convertGPS(gpsinfo[3],"N") + "|"
		times 		+= convertTime(gpsinfo[1],gpsinfo[9])+","
		direction 	+= gpsinfo[8] + ","
		speed 		+= gpsinfo[7] + ","

	#坐标信息转换为高德坐标
	url  = "http://restapi.amap.com/v3/assistant/coordinate/convert?key=58d61ec7efa3e9a4f95cb804e621abce&coordsys=gps&locations="+locations
	
	response = urllib2.urlopen(url)
	autonavi_locations = json.loads(response.read())
	locations = autonavi_locations["locations"].replace(";","|")

	print locations[0:-1] + "\r\n" + times[0:-1] + "\r\n" + direction[0:-1] + "\r\n" + speed[0:-1] + "\r\n"
	url  = "http://restapi.amap.com/v3/autograsp?carid=abceJ80B67&key=58d61ec7efa3e9a4f95cb804e621abce"
	url  += "&locations="+locations[0:-1]
	url  += "&time="+times[0:-1]
	url  += "&direction="+direction[0:-1]
	url  += "&speed="+speed[0:-1]

	url = "http://restapi.amap.com/v3/autograsp?key=58d61ec7efa3e9a4f95cb804e621abce&carid=abcd123456&locations=116.496167,39.917066|116.496149,39.917205|116.496149,39.917326&time=1434077500,1434077501,1434077510&direction=1,1,2&speed=1,1,2"

	response = urllib2.urlopen(url)

	roads = json.loads(response.read())
	
	roadname = ""
	maxspeed = 0
	if roads.has_key("count"):
		for i in range(int(roads["count"])):
			roadname = roads["roads"][i]["roadname"]
			maxspeed = roads["roads"][i]["maxspeed"]
			print "当前道路:",roadname,"限速:",maxspeed




def gps(nmea):
	i = nmea.find("$GPRMC")
	if i < 0:
		return
	j = nmea.find("\r\n",i)
	if j < 0:
		return

	if len(gprmc) >= 3:
		gprmc.pop(0)

	gprmc.append(nmea[i:j])
	#print gprmc
	# list1 = []

	# list1.append("111111")
	# list1.append("222222")
	# list1.append("333333")
	# print list1

	# list1.pop(0)
	# print list1

	# list1.append("444444")
	# print list1

	# #经度
	# print convertGPS(gprmc[5],"E")
	
	# #纬度
	# print convertGPS(gprmc[3],"N")


def connectToGolo():
	HOST="192.168.43.1"
	PORT=7000
	s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)      #定义socket类型，网络通信，TCP
	s.settimeout(5)
	try:
		s.connect((HOST,PORT))       #要连接的IP与端口
		while 1:
			#print "接收数据"
	   		data=s.recv(1024)     #把接收的数据定义为变量
	   		#print(data)
	   		gps(data)
	except Exception,e:
		print Exception,":",e
		s.close()
		return -1
	else:
		print "连接成功"
	s.close()   #关闭连接
	return 0

if __name__ == "__main__":
	t = threading.Thread(target=connectToGolo)
	t.start()


	nmea = ("$GPGGA,005119.000,2838.9910,N,12124.8990,E,1,5,3.85,94.7,M,8.9,M,,*5C\r\n"
				"$GPGSA,A,3,193,17,19,05,23,,,,,,,,3.94,3.85,0.86*3D\r\n"
				"$GPGSV,3,1,10,19,76,066,16,193,75,066,16,17,65,110,35,06,55,349,*41\r\n"
				"$GPGSV,3,2,10,09,38,096,,02,36,302,,05,23,228,18,12,22,308,*7F\r\n"
				"$GPGSV,3,3,10,23,21,058,12,28,11,175,*7D\r\n"
				"$GPRMC,005125.000,A,2838.9922,N,12124.8980,E,0.000,160.18,241116,,,A*5E\r\n"
				"$GPVTG,167.52,T,,M,0.000,N,0.000,K,A*3A\r\n"
				"$GPACCURACY,7.0*0F\r\n")
	#
	#getJoke()
	gps(nmea)
	nmea = ("$GPGGA,005119.000,2838.9910,N,12124.8990,E,1,5,3.85,94.7,M,8.9,M,,*5C\r\n"
				"$GPGSA,A,3,193,17,19,05,23,,,,,,,,3.94,3.85,0.86*3D\r\n"
				"$GPGSV,3,1,10,19,76,066,16,193,75,066,16,17,65,110,35,06,55,349,*41\r\n"
				"$GPGSV,3,2,10,09,38,096,,02,36,302,,05,23,228,18,12,22,308,*7F\r\n"
				"$GPGSV,3,3,10,23,21,058,12,28,11,175,*7D\r\n"
				"$GPRMC,005126.000,A,2838.9922,N,12124.8978,E,0.000,159.50,241116,,,A*5C\r\n"
				"$GPVTG,167.52,T,,M,0.000,N,0.000,K,A*3A\r\n"
				"$GPACCURACY,7.0*0F\r\n")

	gps(nmea)
	nmea = ("$GPGGA,005119.000,2838.9910,N,12124.8990,E,1,5,3.85,94.7,M,8.9,M,,*5C\r\n"
				"$GPGSA,A,3,193,17,19,05,23,,,,,,,,3.94,3.85,0.86*3D\r\n"
				"$GPGSV,3,1,10,19,76,066,16,193,75,066,16,17,65,110,35,06,55,349,*41\r\n"
				"$GPGSV,3,2,10,09,38,096,,02,36,302,,05,23,228,18,12,22,308,*7F\r\n"
				"$GPGSV,3,3,10,23,21,058,12,28,11,175,*7D\r\n"
				"$GPRMC,005127.000,A,2838.9922,N,12124.8976,E,0.000,160.80,241116,,,A*54\r\n"
				"$GPVTG,167.52,T,,M,0.000,N,0.000,K,A*3A\r\n"
				"$GPACCURACY,7.0*0F\r\n")
	gps(nmea)

	

	i = 0
	while 1:
		time.sleep(10)
		getCurrentRoad()
		# if i % 60 == 0:
		# 	getJoke()
		# 	i = 0
		# i += 1


